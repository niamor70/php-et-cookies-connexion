<?php

if ( $_GET['color'] ) {
    setcookie("color", $_GET['color']);
    header('Location: index.php');
}

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body {
            background-color: <?php if ( $_COOKIE['color'] == 'black' ) echo 'black'; else echo 'white'; ?>;
        }
    </style>
</head>
<body>

<a href="index.php?color=white">Blanc</a>
<a href="index.php?color=black">Noir</a>

</body>
</html>