<?php

$db = new PDO("mysql:host=localhost;dbname=courssession", "admin", "admin");

// déconnexion (suppression du cookie)
if (isset($_GET['logout'])) {
    setcookie('loggedin');
    header('Location: session.php');
}

// connexion : si on a un login et un mot de passe
if (isset($_POST['login']) && isset($_POST['password'])) {
    // on va chercher l'utilisateur dans la base de données
    $user = $db->prepare("SELECT * FROM user WHERE username = :username");
    $user->execute([
        'username' => $_POST['login'],
    ]);

    // si l'utilisateur existe
    if ($user->rowCount() > 0) {
        $user = $user->fetch();
        // on vérifie le mot de passe
        if (password_verify($_POST['password'], $user['password'])) {
            // on créer la session en base de données + le cookie
            $session_id = md5(uniqid(rand(), true));
            $insertSession = $db->prepare("INSERT INTO session (id, last_access, user_id) VALUES (:id, :last_access, :user_id) ");
            $insertSession->execute([
                'id' => $session_id,
                'last_access' => time(),
                'user_id' => $user['id'],
            ]);
            setcookie("loggedin", $session_id);
            header('Location: session.php');
        } else {
            echo "problème de connexion";
        }
    } else {
        echo "problème de connexion";
    }

}


?><!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Utilisation des cookies pour la connexion</title>
</head>
<body>

<?php
// on vérifie si l'utilisateur est connecté ou pas
if ($_COOKIE['loggedin']) {
    $session = $db->prepare("SELECT u.username FROM session s, user u WHERE s.id = :id AND s.user_id = u.id");
    $session->execute([
        'id' => $_COOKIE['loggedin'],
    ]);
    if ($session->rowCount() > 0) {
        $session = $session->fetch();
        echo 'on est connecté avec le compte ' . $session['username'];
    }

    echo '<a href="session.php?logout">Déconnexion</a>';
} else {
    ?>

    <form action="session.php" method="POST">
        identifiant: <input type="text" name="login"><br>
        mot de passe: <input type="password" name="password"><br>
        <input type="submit" value="Connexion">
    </form>

    <?php
}
?>

</body>
</html>
